/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package robotcar;


import carpositions.CarPosition;
import carpositions.LanePosition;
import carpositions.PiecePosition;
import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import robotcar.info.CarInfo;
import robotcar.info.RaceInfo;
import robotcar.info.TrackInfo;
import time.TimeHandler;
import track.TrackPiece;

/**
 *
 * @author chakku
 */
public class RobotCar {
    
    // Informa��es b�sicas do carro (nome e cor)
    // Utilizadas na identifica��o do carro na pista
    private CarInfo carInfo;
    private RaceInfo raceInfo;
    
    
    // Informa��o de posicionamento de todos os carros na pista
    private CarPosition[] carPositions;
    
    // Infoma��o do posicionamento do meu carro na pista
    private CarPosition myCarPosition;
    
    private String gameId;
    private double gameTick;
    
    // Throttle atual do carro
    private double throttle;
    
    
    // Dist�ncia que o carro j� andou desde o come�o da
    // (Soma a dist�ncia percorrida em todas as voltas)
    private double movedDistance;
    
    // Velocidade atual do carro (dist�ncia/tempo)
    private double speed;
    
    // ID do �tlimo switch (peda�o da pista) onde o jogador
    // mudou de lane
    private double lastSwitchIndex;
    
    private TimeHandler timeHandler;
    
    private boolean outOfTrack;
    
    
    public RobotCar(TimeHandler timeHandler) {
        this.outOfTrack = false;
        this.timeHandler = timeHandler;
        this.movedDistance = 0;
        this.lastSwitchIndex = -1;
        
        this.throttle = 1f;
    }
    
    public void setCarInfoFromServerData(LinkedTreeMap data) {
        this.carInfo = new CarInfo(data);
    }

    final double MAX_SPEED = 9;
    final double MID_SPEED = 8;
    final double MIN_SPEED = 6;
    final double MAX_CURVE_SPEED = 6;
    
    final double MAX_THROTTLE = 1;
    final double MID_THROTTLE = 0.85;
    final double MIN_THROTTLE = 0.47;
    final double BEFORE_CURVE_THROTTLE = 0.5;
    final double CURVE_THROTTLE = 0.65;
    
    final double TURNOFF_THROTTLE = 0.3;
    
    public double getThrottle() {                
        // return 0.6496;
        
        TrackPiece currentTrackPiece = getMyCurrentTrackPiece();
        TrackPiece nextTrackPiece = currentTrackPiece.getNext();
        
     
        // Estou em uma RETA
        if (currentTrackPiece.isStraight()) {
            // O pr�ximo � uma RETA
            if (nextTrackPiece.isStraight()) {
                // O pr�ximo, pr�ximo � uma RETA
                if (nextTrackPiece.getNext().isStraight()) {
                    if (Math.round(this.speed) < MAX_SPEED) {
                        this.throttle = MAX_THROTTLE;
                    } else {
                        this.throttle = MID_THROTTLE;
                    }
                } else {
                    //if (this.speed < MAX_CURVE_SPEED) {
                    //if (Math.round(this.speed) < MID_SPEED) {
                    if (Math.round(this.speed) < MAX_CURVE_SPEED) {                            
                        this.throttle = CURVE_THROTTLE;
                    } else {
                        // O pr�ximo, pr�ximo � uma CURVA
                        this.throttle = BEFORE_CURVE_THROTTLE;
                    }
                }
            } else {
                // O pr�ximo � uma CURVA


                if (Math.round(this.speed) <= MAX_CURVE_SPEED) {                
                    this.throttle = CURVE_THROTTLE;
                } else {
                    
                    // Ajustar aqui
                    // Valor tem que ser maior que MIN_THROTTLE(suficiente para passar) e MENOR que MID_THROTTLE(sai da pista)
                    
                    this.throttle = MIN_THROTTLE;
                }

                // Se o pr�ximo, pr�ximo tamb�m for CURVA
                /*if (nextTrackPiece.getNext().isCurve()) {                        
                    //if (Math.round(this.speed) > MAX_CURVE_SPEED) {
                    //    this.throttle = MIN_THROTTLE;
                    //}

                    // aqui

                    if (Math.round(this.speed) <= MAX_CURVE_SPEED) {
                        this.throttle = CURVE_THROTTLE;
                    } else {
                        this.throttle = MIN_THROTTLE;
                    }
                } else {
                    if (Math.round(this.speed) <= MAX_CURVE_SPEED) {
                        this.throttle = CURVE_THROTTLE;
                    } else {
                        this.throttle = TURNOFF_THROTTLE;
                    }
                }*/
            }
        } else {
            // Estou dentro da CURVA                        


            // O pr�ximo � uma RETA
            if (nextTrackPiece.isStraight()) {
                // Caso esteja ABAIXO da velocidade M�DIA
                //if (Math.round(this.speed) < MID_SPEED) {
                if (Math.round(this.speed) < MAX_SPEED) {
                    this.throttle = MAX_THROTTLE;
                } else {
                    this.throttle = MID_THROTTLE;
                }
            } else {
                if (Math.round(this.speed) >= MID_SPEED) {
                    System.out.println("\n\n\n======================================\nAtingiu Throttle ZERO\n======================================\n\n\n");
                    this.throttle = 0;
                } else {

                    // Caso esteja ABAIXO da velocidade M�XIMA em CURVA
                    if (Math.round(this.speed) < MAX_CURVE_SPEED) {
                        // Caso esteja saindo de uma grande redu��o de velocidade
                        if (this.throttle == 0) {
                            this.throttle = MID_THROTTLE;
                        } else {
                            this.throttle = CURVE_THROTTLE;
                        }
                    } else {
                        // Caso esteja na VELOCIDADE M�XIMA em CURVA
                        if (Math.round(this.speed) == MAX_CURVE_SPEED) {
                            this.throttle = CURVE_THROTTLE;
                        } else {
                            // Est� ACIMA da velocidade M�XIMA em CURVA
                            this.throttle = TURNOFF_THROTTLE;
                        }
                    }

                }
            }

        }
            
        return this.throttle;
    }

    public void setRaceInfoFromServerData(LinkedTreeMap data) {
        this.raceInfo = new RaceInfo(data);
    }
    
    // M�todo chamado a cada "loop" da corrida
    public void onUpdate(ArrayList data, String gameId, double gameTick) {
        // Atualiza as informa��es das posi��es dos carros
        updateCarPositions(data);
        
        // Atualiza a ID do jogo
        // TODO: Verificar se � necess�rio
        setGameId(gameId);
        
        // Atualiza o Game Tick
        setGameTick(gameTick);
        
        
        
        // Implementar l�gica de decis�o aqui
        
        
        
        
    }
    
    public void onCrash(LinkedTreeMap data) {
        String color = (String)data.get("color");
        if (isItMe(color)) {
            this.outOfTrack = true;
            System.out.println("\n\n\nMeu carro saiu da pista.\n\n\n");
        } else {
            String name = (String)data.get("name");
            System.out.println(name + "(" + color + ") saiu da pista.");
        }
    }
    
    public void onSpawn(LinkedTreeMap data) {
        String color = (String)data.get("color");
        if (isItMe(color)) {
            this.outOfTrack = false;
            System.out.println("\n\n\nMeu carro voltou para a pista.\n\n\n");
        } else {
            String name = (String)data.get("name");
            System.out.println(name + "(" + color + ") voltou para a pista.");
        }
    }
    
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
    
    public void setGameTick(double gameTick) {
        this.gameTick = gameTick;
    }

    /**
     * @return the carInfo
     */
    public CarInfo getCarInfo() {
        return carInfo;
    }

    /**
     * @return the raceInfo
     */
    public RaceInfo getRaceInfo() {
        return raceInfo;
    }
    
    public TrackInfo getTrackInfo() {
        if (getRaceInfo() != null) {
            return getRaceInfo().getTrackInfo();
        }
        return null;
    }
    
    public TrackPiece[] getTrackPieces() {
        if (getTrackInfo() != null) {
            return getTrackInfo().getTrackPieces();
        }
        return null;
    }
    
    /**
     * Retorna a representa��o das informa��es de um peda�o da pista
     * de acordo com o �ndice passado por par�metro
     * @param index
     * @return 
     */
    public TrackPiece getTrackPiece(double index) {
        if (getTrackPieces() != null) {
            return getTrackPieces()[(int)index];
        }
        return null;
    }
    
    /**
     * Retorna o peda�o da pista onde MEU carro est� atualmente
     * @return 
     */
    public TrackPiece getMyCurrentTrackPiece() {
        return getTrackPiece(getMyCurrentTrackPieceIndex());
    }
    
    public PiecePosition getMyPiecePosition() {
        if (getMyCarPosition() != null) {
            return getMyCarPosition().getPiecePosition();
        }
        return null;
    }
    
    public double getMyCurrentTrackPieceIndex() {
        return getMyPiecePosition().getPieceIndex();
    }

    /**
     * @return the carPositions
     */
    public CarPosition[] getCarPositions() {
        return carPositions;
    }

    /**
     * @return the gameId
     */
    public String getGameId() {
        return gameId;
    }

    /**
     * @return the gameTick
     */
    public double getGameTick() {
        return gameTick;
    }

    public int switchLane() {
        TrackPiece nextTrackPiece = getMyCurrentTrackPiece().getNext();
        
        if (nextTrackPiece.getTrackPieceIndex() != this.lastSwitchIndex) {        
            if (nextTrackPiece.isSwitch()) {
                // Pr�xima curva da corrida
                TrackPiece currentCurve = getTrackInfo().getNextCurve((int)nextTrackPiece.getTrackPieceIndex());                                
                
                TrackPiece nextCurve = getTrackInfo().getNextCurve((int)currentCurve.getTrackPieceIndex());
                boolean hasOtherSwitch = getTrackInfo().hasSwitchBetween((int)currentCurve.getTrackPieceIndex(), (int)nextCurve.getTrackPieceIndex());
                
                // Curva que ser� utilizada para tomar a decis� ode mudan�a de pista
                TrackPiece curve = null;
                
                if (!hasOtherSwitch) {
                    // S�o curvas em dure��es opostas
                    if (((nextCurve.getAngle() < 0) && (currentCurve.getAngle() > 0)) || ((nextCurve.getAngle() > 0) && (currentCurve.getAngle() < 0))) {
                        // A pr�xima curva tem angula��o IGUAL ou MAIOR
                        if (Math.abs(nextCurve.getAngle()) >= Math.abs(currentCurve.getAngle())) {
                            curve = nextCurve;
                        }
                    }
                }
                
                if (curve == null) {
                    curve = currentCurve;
                }
                
                // Curva para a direita
                if (curve.getAngle() > 0) {
                    if (getTrackInfo().hasRightLane(getMyCurrentLaneIndex())) {
                        this.lastSwitchIndex = nextTrackPiece.getTrackPieceIndex();
                        return 1;
                    }
                } else {
                    // Curva para a esquerda
                    if (curve.getAngle() < 0) {
                        if (getTrackInfo().hasLeftLane(getMyCurrentLaneIndex())) {
                            this.lastSwitchIndex = nextTrackPiece.getTrackPieceIndex();
                            return -1;
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    public int getMyCurrentLaneIndex() {
        // TODO: Verificar se � melhor utilizar o startIndex ou endIndex
        return (int)getMyLanePosition().getEndLaneIndex();
    }
    
    public LanePosition getMyLanePosition() {
        if (getMyPiecePosition() != null) {
            return getMyPiecePosition().getLanePosition();
        }
        return null;
    }

    /**
     * @return the myCarPosition
     */
    public CarPosition getMyCarPosition() {
        return myCarPosition;
    }
    
    private void updateCarPositions(ArrayList data) {
        int carsCount = data.size();
        
        this.carPositions = new CarPosition[carsCount];
                
        CarPosition carPosition;
        for (int i = 0; i < carsCount; i++) {            
            carPosition = new CarPosition((LinkedTreeMap)data.get(i));
            this.carPositions[i] = carPosition;
            
            // Caso sejam as informa��es do MEU carro
            if (getCarInfo().equals(carPosition.getCarInfo())) {
                PiecePosition piecePosition = carPosition.getPiecePosition();
                int completedLaps = (int)piecePosition.getLap();
                if ((getMyPiecePosition() == null) || ((getMyPiecePosition() != null) && (getMyPiecePosition().getLap() < completedLaps))) {                    
                    System.out.println("Volta atual: " + completedLaps);
                }
                
                
                
                
                // Armazena a dist�ncia atual percorrida pelo MEU carro
                double currentMovedDistance = this.movedDistance;
                
                // Calcula a dist�ncia percorrida pelo MEU carro
                // desde o �ltimo GAME LOOP
                calculateMovedDistance(piecePosition);
                
                // Calcula a dist�ncia percorrida pelo MEU carro no GAME LOOP atual
                double elapsedDistance = Math.abs(this.movedDistance - currentMovedDistance);        
                // Calcula a velocidade atual do MEU carro
                calculateSpeed(elapsedDistance);
                
                this.myCarPosition = carPosition;
                
                if (!this.outOfTrack && (Math.abs(getMyCurrentTrackPiece().getAngle()) > 0)) {
                    System.out.println("�ngulo do carro: " + getMyCarPosition().getAngle() + " �ngulo da curva: " + getMyCurrentTrackPiece().getAngle() + "/" + getMyCurrentTrackPiece().getRadius());
                }
            }            
        }
    }
    
    private void calculateMovedDistance(PiecePosition piecePosition) {
        if (getMyPiecePosition() != null) {
            // Caso esteja no mesmo peda�o da pista do �ltimo GAME LOOP
            if (getMyPiecePosition().getPieceIndex() == piecePosition.getPieceIndex()) {
                this.movedDistance += Math.abs(piecePosition.getInPieceDistance() - getMyPiecePosition().getInPieceDistance());
            } else {
                this.movedDistance += piecePosition.getInPieceDistance();
            }
        } else {
            this.movedDistance = piecePosition.getInPieceDistance();
        }
    }
    
    /**
     * Identifica se a cor passada por par�metro
     * � a mesma cor do MEU carro
     * @param color
     * @return 
     */
    private boolean isItMe(String color) {
        return (boolean)(getCarInfo().getColor().equals(color));
    }
    
    
    // Tentativa de obter um valor mais "real" de velocidade
    // com base na m�dia dos �ltimos 3 loops
    double[] s = new double[]{0, 0, 0};
    int currentSpeedIndex = 0;
    
    private void calculateSpeed(double distance) {
        double currentSpeed = this.speed;
        
        
        s[currentSpeedIndex] = Math.abs(distance / timeHandler.getElapsedTime());
        currentSpeedIndex++;
        
        if (currentSpeedIndex == s.length) {
            currentSpeedIndex = 0;
        }
        
        double newSpeed = 0;
        for (int i = 0; i < s.length; i++) {
            newSpeed += s[i];
        }
        newSpeed /= s.length;
                
        this.speed = newSpeed;
        
        //this.speed = Math.abs(distance / timeHandler.getElapsedTime());
        if ((this.speed > 0) && (Math.round(this.speed) != Math.round(currentSpeed))) {
            System.out.print("Velocidade atual: " + Math.round(this.speed) + " units/gt (" + getThrottle() + ")");            
            System.out.println(" - " + (getMyCurrentTrackPiece().isStraight() ? "reta" : "curva") + " seguinda de " + (getMyCurrentTrackPiece().getNext().isStraight() ? "reta" : "curva"));
        }
    }
    
}
