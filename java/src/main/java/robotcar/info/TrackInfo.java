/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package robotcar.info;

import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import race.Car;
import track.Lane;
import race.RaceSession;
import track.StartingPoint;
import track.TrackPiece;

/**
 *
 * @author chakku
 */
public class TrackInfo {
    
    private String id;
    private String name;
    
    private TrackPiece[] trackPieces;
    private Lane[] lanes;
    private StartingPoint startingPoint;
    
    
    
    public TrackInfo(LinkedTreeMap track) {
        
        this.id = (String)track.get("id");
        this.name = (String)track.get("name");
        
        this.trackPieces = getPiecesFromTrack(track);
        this.lanes = getLanesFromTrack(track);
 
        this.startingPoint = new StartingPoint((LinkedTreeMap)track.get("startingPoint"));
    }
    
    
    private TrackPiece[] getPiecesFromTrack(LinkedTreeMap track) {        
        ArrayList pieces = (ArrayList)track.get("pieces");
        
        int piecesCount = pieces.size();
        TrackPiece[] trackPieces = new TrackPiece[piecesCount];
        
        TrackPiece piece = null;
        TrackPiece nextTrackPiece = null;
        TrackPiece lastTrackPiece = null;
        for (int i = (piecesCount - 1); i >= 0; i--) {
            piece = new TrackPiece((LinkedTreeMap)pieces.get(i), nextTrackPiece, i);
            
            // Obt�m a refer�ncia para o �LTIMO TrackPiece
            // (para setar a refer�ncia do "next" ao fim do loop
            if (lastTrackPiece == null) {
                lastTrackPiece = piece;
            }
            
            nextTrackPiece = piece;
            trackPieces[i] = piece;
        }
        
        // Caso exista a refer�ncia para o �LTIMO e o PRIMEIRO
        // TrackPiece, seta a refer�ncia do NEXT do �ltimo
        // como sendo o primeiro
        if ((lastTrackPiece != null) && (piece != null)) {
            lastTrackPiece.setNext(piece);
        }
        
        return trackPieces;
    }
    
    public boolean hasSwitchBetween(int currentTrackIndex, int targetTrackIndex) {
        int trackPieceCount = this.trackPieces.length;
        
        double index;
        
        // Caso esteja no �ltimo peda�o da pista
        if (currentTrackIndex == (trackPieceCount - 1)) {
            index = 0;
        } else {
            index = (currentTrackIndex + 1);
        }
        
        // Contador para evitar loop infinito
        int safeBreakCount = 0;
        
        while (index != currentTrackIndex) {            
            if (this.trackPieces[(int)index].isSwitch()) {
                if (((currentTrackIndex >= index) && (targetTrackIndex <= index)) || ((currentTrackIndex <= index) && (targetTrackIndex >= index))) {
                    return true;
                }
            }
            
            if (index == (trackPieceCount - 1)) {
                index = 0;
            } else {
                index++;
            }
                        
            if (safeBreakCount > trackPieceCount) {
                System.out.println("[TrackInfo]: {hasSwitchBetween} Loop infinito. Condicional de seguran�a atingida.");
                break;
            }
            safeBreakCount++;
        }
        
        return false;
    }
    
    public TrackPiece getNextCurve(int currentTrackIndex) {
        int trackPieceCount = this.trackPieces.length;
        
        double index;
        
        // Caso esteja no �ltimo peda�o da pista
        if (currentTrackIndex == (trackPieceCount - 1)) {
            index = 0;
        } else {
            index = (currentTrackIndex + 1);
        }
        
        // Contador para evitar loop infinito
        int safeBreakCount = 0;
        
        while (index != currentTrackIndex) {
            if (this.trackPieces[(int)index].isCurve()) {
                return this.trackPieces[(int)index];
            }
            
            if (index == (trackPieceCount - 1)) {
                index = 0;
            } else {
                index++;
            }
                        
            if (safeBreakCount > trackPieceCount) {
                System.out.println("[TrackInfo]: {getNextCurve} Loop infinito. Condicional de seguran�a atingida.");
                break;
            }
            safeBreakCount++;
        }
        
        return null;
    }
    
    private Lane[] getLanesFromTrack(LinkedTreeMap track) {        
        ArrayList lanes = (ArrayList)track.get("lanes");
        
        int lanesCount = lanes.size();
        Lane[] trackLanes = new Lane[lanesCount];
        
        Lane lane = null;        
        for (int i = 0; i < lanesCount; i++) {
            lane = new Lane((LinkedTreeMap)lanes.get(i));
            trackLanes[i] = lane;
        }
        
        return trackLanes;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the trackPieces
     */
    public TrackPiece[] getTrackPieces() {
        return trackPieces;
    }

    /**
     * @return the lanes
     */
    public Lane[] getLanes() {
        return lanes;
    }
    
    /**
     * Verifica se existe mais uma pista ao lado DIREITO do carro
     * (A dire��o para a qual o carro est� andando afeta o lado right/left)
     * @return 
     */
    public boolean hasRightLane(int currentLaneIndex) {
        int lanesCount = lanes.length;
        for (int i = 0; i < lanesCount; i++) {
            if (lanes[i].getIndex() > currentLaneIndex) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Verifica se existe mais uma pista ao lado ESQUERDO do carro
     * (A dire��o para a qual o carro est� andando afeta o lado right/left)
     * @return 
     */
    public boolean hasLeftLane(int currentLaneIndex) {
        int lanesCount = lanes.length;
        for (int i = 0; i < lanesCount; i++) {
            if (lanes[i].getIndex() < currentLaneIndex) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the startingPoint
     */
    public StartingPoint getStartingPoint() {
        return startingPoint;
    }
    
}
