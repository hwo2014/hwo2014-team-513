/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package robotcar.info;

import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import race.Car;
import race.RaceSession;

/**
 *
 * @author chakku
 */
public class RaceInfo {
    
    private TrackInfo trackInfo;
    private Car[] cars;
    
    private RaceSession raceSession;
    
    
    public RaceInfo(LinkedTreeMap data) {
        LinkedTreeMap race = (LinkedTreeMap)data.get("race");
        LinkedTreeMap track = (LinkedTreeMap)race.get("track");
        
        this.trackInfo = new TrackInfo(track);
        
        this.cars = getCarsFromRace(race);
        this.raceSession = new RaceSession((LinkedTreeMap)race.get("raceSession"));
    }
    
    private Car[] getCarsFromRace(LinkedTreeMap race) {        
        ArrayList cars = (ArrayList)race.get("cars");
        
        int carsCount = cars.size();
        Car[] trackCars = new Car[carsCount];
        
        Car car = null;        
        for (int i = 0; i < carsCount; i++) {
            car = new Car((LinkedTreeMap)cars.get(i));
            trackCars[i] = car;
        }
        
        return trackCars;
    }

    /**
     * @return the trackInfo
     */
    public TrackInfo getTrackInfo() {
        return trackInfo;
    }

    /**
     * @return the cars
     */
    public Car[] getCars() {
        return cars;
    }

    /**
     * @return the raceSession
     */
    public RaceSession getRaceSession() {
        return raceSession;
    }
    
}
