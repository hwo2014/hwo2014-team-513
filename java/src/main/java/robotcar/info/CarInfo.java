/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package robotcar.info;

import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;

/**
 *
 * @author chakku
 */
public class CarInfo {
    
    // Nome do carro
    private final String name;
    
    // Cor do carro
    private final String color;
    
    
    
    public CarInfo(LinkedTreeMap data) {
        this.name = (String)data.get("name");
        this.color = (String)data.get("color");        
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }
    
    public boolean equals(CarInfo carInfo) {
        return (getColor().equals(carInfo.getColor()));
    }
    
}
