/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package time;

/**
 *
 * @author chakku
 */
public class TimeHandler {
    
    private double elapsedTime;
    private double lastFrameTime;
    
    
    public TimeHandler() {
        this.elapsedTime = 0;
        this.lastFrameTime = 0;
    }
    
    
    
    public void update(double gameTick) {
        if (this.lastFrameTime == 0) {
            this.lastFrameTime = gameTick;
        }
        this.elapsedTime = (gameTick - this.lastFrameTime);
        this.lastFrameTime = gameTick;
    }
    
    public double getElapsedTime() {
        return this.elapsedTime;
    }
    
    
}
