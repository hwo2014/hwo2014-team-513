package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import robotcar.RobotCar;
import robotcar.info.CarInfo;
import time.TimeHandler;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        
        /*String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "VilliTrue";
        String botKey = "Y19XjoATQJUJAA";*/
        

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        TimeHandler timeHandler = new TimeHandler();
        RobotCar car = new RobotCar(timeHandler);
        
        send(join);
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);            
//            System.out.println("ServerMessage: " + msgFromServer.data);            
            
            if (msgFromServer.msgType.equals("carPositions")) {
                final LinkedTreeMap extraInfo = gson.fromJson(line, LinkedTreeMap.class);
                
                String gameId = null;
                if (extraInfo.containsKey("gameId")) {
                    gameId = (String)extraInfo.get("gameId");
                }
                
                double gameTick = -1;
                if (extraInfo.containsKey("gameTick")) {
                    gameTick = (double)extraInfo.get("gameTick");
                    // Atualiza o tempo decorrido no jogo
                    timeHandler.update(gameTick);
                }
                
                car.onUpdate((ArrayList)msgFromServer.data, gameId, gameTick);                
                
                switch(car.switchLane()) {
                    case 0:
                        send(new Throttle(car.getThrottle()));
                        break;
                    case 1:
                        send(SwitchLane.switchToRightLane());
                        System.out.println("Ir para direita.");
                        break;
                    case -1:
                        send(SwitchLane.switchToLeftLane());
                        System.out.println("Ir para esquerda.");
                        break;
                }
                
                //send(new Throttle(car.getThrottle()));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                // Descreve a pista de corrida, a sess�o atual
                // e os carros na pista.                
                // A  pista � descrita como um Array de "pieces" que podem ser retas ou curvas                
                car.setRaceInfoFromServerData((LinkedTreeMap)msgFromServer.data);
                
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                // Fim da corrida
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                // Corrida iniciada
                System.out.println("Race start");                
            } else {          
                
                
                // Informa��es do carro
                if (msgFromServer.msgType.equals("yourCar")) {
                    car.setCarInfoFromServerData((LinkedTreeMap)msgFromServer.data);
                } else {
                    if (msgFromServer.msgType.equals("crash")) {                    
                        car.onCrash((LinkedTreeMap)msgFromServer.data);
                    } else {
                        if (msgFromServer.msgType.equals("spawn")) {
                            car.onSpawn((LinkedTreeMap)msgFromServer.data);
                        } else {
                            if (msgFromServer.msgType.equals("dnf")) {
                                LinkedTreeMap data = (LinkedTreeMap)msgFromServer.data;
                                System.out.println("[Desclassificado] Raz�o: " + (String)data.get("reason"));
                            } else {
                                if (msgFromServer.msgType.equals("lapFinished")) {
                                    LinkedTreeMap data = (LinkedTreeMap)msgFromServer.data;
                                    CarInfo carInfo = new CarInfo(((LinkedTreeMap)data.get("car")));
                                    // Caso MEU carro tenha conclu�do a volta
                                    if (car.getCarInfo().equals(carInfo)) {
                                        LinkedTreeMap lapTime = (LinkedTreeMap)data.get("lapTime");
                                        double lap = (double)lapTime.get("lap");
                                        double millis = (double)lapTime.get("millis");
                                        double time = millis/1000;
                                        
                                        System.out.println("\n\n\nVolta " + lap + " finalizada em " + time + " segundos.\n\n\n");
                                    }
                                } else {
                                    System.out.println("[Main] {Main} Mensagem N�O tratada: " + msgFromServer.msgType);
                                }
                            }
                        }
                    }
                }
                
                
                
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;
    
    public static SwitchLane switchToLeftLane() {
        return new SwitchLane("Left");
    }
    
    public static SwitchLane switchToRightLane() {
        return new SwitchLane("Right");
    }
    
    SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    
}