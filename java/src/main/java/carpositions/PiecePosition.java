/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package carpositions;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class PiecePosition {
    
    private double pieceIndex;
    private double inPieceDistance;
    
    private LanePosition lanePosition;
    
    // Quantidade de voltas conclu�das
    private double lap;
    
    
    
    public PiecePosition(LinkedTreeMap piecePosition) {
        this.pieceIndex = (double)piecePosition.get("pieceIndex");
        this.inPieceDistance = (double)piecePosition.get("inPieceDistance");
        
        this.lanePosition = new LanePosition((LinkedTreeMap)piecePosition.get("lane"));
        
        this.lap = (double)piecePosition.get("lap");
    }

    /**
     * @return the pieceIndex
     */
    public double getPieceIndex() {
        return pieceIndex;
    }

    /**
     * @return the inPieceDistance
     */
    public double getInPieceDistance() {
        return inPieceDistance;
    }

    /**
     * @return the lanePosition
     */
    public LanePosition getLanePosition() {
        return lanePosition;
    }

    /**
     * @return Retorna a quantidade de voltas j� conclu�das
     */
    public double getLap() {
        return lap;
    }
    
    
}
