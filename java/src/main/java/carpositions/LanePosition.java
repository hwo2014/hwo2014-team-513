/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package carpositions;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class LanePosition {
    
    private double startLaneIndex;
    private double endLaneIndex;
    
    
    public LanePosition(LinkedTreeMap lane) {
        this.startLaneIndex = (double)lane.get("startLaneIndex");
        this.endLaneIndex = (double)lane.get("endLaneIndex");
    }

    /**
     * @return the startLaneIndex
     */
    public double getStartLaneIndex() {
        return startLaneIndex;
    }

    /**
     * @return the endLaneIndex
     */
    public double getEndLaneIndex() {
        return endLaneIndex;
    }
    
    
    
}
