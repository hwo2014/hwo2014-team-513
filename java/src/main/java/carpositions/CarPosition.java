/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package carpositions;

import com.google.gson.internal.LinkedTreeMap;
import robotcar.info.CarInfo;

/**
 *
 * @author chakku
 */
public class CarPosition {
    
    
    private CarInfo carInfo;
    
    private double angle;
    private PiecePosition piecePosition;
    
    
    public CarPosition(LinkedTreeMap carPosition) {
        this.carInfo = new CarInfo((LinkedTreeMap)carPosition.get("id"));
        this.angle = (double)carPosition.get("angle");
        this.piecePosition = new PiecePosition((LinkedTreeMap)carPosition.get("piecePosition"));        
    }

    /**
     * @return the carInfo
     */
    public CarInfo getCarInfo() {
        return carInfo;
    }

    /**
     * @return the angle
     */
    public double getAngle() {
        return angle;
    }

    /**
     * @return the piecePosition
     */
    public PiecePosition getPiecePosition() {
        return piecePosition;
    }
    
    
    
    
    
    
    
}
