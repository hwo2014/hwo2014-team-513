/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package track;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class Lane {
    
    private double distanceFromCenter;
    private double index;
    
    
    
    public Lane(LinkedTreeMap lane) {
        this.distanceFromCenter = (double)lane.get("distanceFromCenter");
        this.index = (double)lane.get("index");
    }

    /**
     * @return the distanceFromCenter
     */
    public double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    /**
     * @return the index
     */
    public double getIndex() {
        return index;
    }
    
    
    
}
