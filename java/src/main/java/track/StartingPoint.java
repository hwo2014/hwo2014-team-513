/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package track;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class StartingPoint {
    
    private double x;
    private double y;
    
    private double angle;

    
    
    public StartingPoint(LinkedTreeMap startingPoint) {
        LinkedTreeMap positions = (LinkedTreeMap)startingPoint.get("position");
        this.x = (double)positions.get("x");
        this.y = (double)positions.get("y");
        
        this.angle = (double)startingPoint.get("angle");
    }   
    
    
    
    
    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @return the angle
     */
    public double getAngle() {
        return angle;
    }
    
    
    
}
