/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package track;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class TrackPiece {
    
    // Comprimento do peda�o da pista
    private double length;
    
    // Identifica se o peda�o da pista possui um SWITCH para troca de pista
    private boolean _switch;
    
    // Raio da curva (caso seja uma curva)
    private double radius;
    
    // �ngulo da curva (caso seja uma curva)
    private double angle;
    
    private double trackPieceIndex;
    
    // Prox�mo TrackPiece da pista (ap�s o TrackPiece atual)
    private TrackPiece next;
    
    
    public TrackPiece(LinkedTreeMap piece, TrackPiece next, double trackPieceIndex) {
        this.trackPieceIndex = trackPieceIndex;
        this.next = next;
        
        if (piece.containsKey("length")) {
            this.length = (double)piece.get("length");
        } else {
            this.length = -1;
        }
        
        if (piece.containsKey("switch")) {
            this._switch = (boolean)piece.get("switch");
        } else {
            this._switch = false;
        }
        
        if (piece.containsKey("radius")) {
            this.radius = (double)piece.get("radius");
        } else {
            this.radius = 0;
        }
        
        if (piece.containsKey("angle")) {
            this.angle = (double)piece.get("angle");
        } else {
            this.angle = 0;
        }
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @return the _switch
     */
    public boolean isSwitch() {
        return _switch;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @return the angle
     */
    public double getAngle() {
        return angle;
    }
    
    public boolean isStraight() {
        return ((getAngle() == 0) && (getRadius() == 0));        
    }
    
    public boolean isCurve() {
        return !isStraight();
    }

    /**
     * @return the next
     */
    public TrackPiece getNext() {
        return next;
    }

    /**
     * @param next the next to set
     */
    public void setNext(TrackPiece next) {
        this.next = next;
    }

    /**
     * @return the trackPieceIndex
     */
    public double getTrackPieceIndex() {
        return trackPieceIndex;
    }
    
    
    
}
