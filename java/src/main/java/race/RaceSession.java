/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package race;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class RaceSession {
    
    private final double laps;
    private final double maxLapTimeMs;
    private final boolean quickRace;
    
    
    
    public RaceSession(LinkedTreeMap raceSession) {
        if (raceSession.containsKey("laps")) {
            this.laps = (double)raceSession.get("laps");
        } else {
            this.laps = 0;
        }
        if (raceSession.containsKey("maxLapTimeMs")) {
            this.maxLapTimeMs = (double)raceSession.get("maxLapTimeMs");
        } else {
            this.maxLapTimeMs = 0;
        }
        if (raceSession.containsKey("quickRace")) {
            this.quickRace = (boolean)raceSession.get("quickRace");
        } else {
            this.quickRace = false;
        }
    }

    /**
     * @return the laps
     */
    public double getLaps() {
        return laps;
    }

    /**
     * @return the maxLapTimeMs
     */
    public double getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    /**
     * @return the quickRace
     */
    public boolean isQuickRace() {
        return quickRace;
    }
    
    
}
