/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package race;

import com.google.gson.internal.LinkedTreeMap;

/**
 *
 * @author chakku
 */
public class Car {
    
    private final String name;
    private final String color;
    
    private final double length;
    private final double width;
    private final double guideFlagPosition;
    
    
    
    public Car(LinkedTreeMap car) {
        LinkedTreeMap id = (LinkedTreeMap)car.get("id");        
        this.name = (String)id.get("name");
        this.color = (String)id.get("color");
        
        LinkedTreeMap dimensions = (LinkedTreeMap)car.get("dimensions");
        this.length = (double)dimensions.get("length");
        this.width = (double)dimensions.get("width");
        this.guideFlagPosition = (double)dimensions.get("guideFlagPosition");
        
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @return the guideFlagPosition
     */
    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }
    
    
    
    
    
}
